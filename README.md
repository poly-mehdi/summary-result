# Frontend Mentor - Results summary component solution

This is a solution to the [Results summary component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/results-summary-component-CE_K6s0maV). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Useful resources](#useful-resources)
- [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page

### Screenshot

![Mobile Design](./design/mobile-design.png)
![Desktop Design](./design/desktop-design.png)

### Links

- Live Site URL: [Result-Summary](https://summary-result.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow

### What I learned

Through this project, I have learned several valuable concepts and techniques in web development. Firstly, I gained a solid understanding of semantic HTML5 markup, which involves using HTML elements that convey meaning and structure to both developers and assistive technologies. This practice enhances accessibility and provides a more organized code structure.

Additionally, I explored the power of CSS custom properties, also known as CSS variables. These variables allowed me to define reusable values and easily update them throughout the entire stylesheet. By leveraging CSS custom properties, I achieved a more efficient and maintainable codebase.

Furthermore, I delved into Flexbox, a CSS layout module that enables flexible and responsive design. With Flexbox, I learned how to create dynamic and adaptive layouts by efficiently aligning and distributing elements along a single axis or in multiple dimensions.

In addition to Flexbox, I also delved into CSS Grid, a powerful grid-based layout system. CSS Grid provided me with the ability to create complex and responsive grid structures with ease. It allowed me to define both rows and columns, control their sizing and alignment, and position elements within the grid.

Lastly, I embraced the mobile-first workflow, an approach to web design and development that prioritizes designing and optimizing for mobile devices before scaling up to larger screens. By adopting this workflow, I ensured that my projects were accessible and visually appealing on a wide range of devices, providing a seamless user experience.

## Author

- Website - [Mon Portofolio](https://portofolio-poly-mehdi.vercel.app/)
- Frontend Mentor - [@poly-mehdi](https://www.frontendmentor.io/profile/poly-mehdi)
